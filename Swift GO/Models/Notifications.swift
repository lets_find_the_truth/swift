//
//  Notifications.swift
//  Swift GO
//
//  Created by Kyryl Nevedrov on 27/03/17.
//  Copyright © 2017 Kyryl Nevedrov. All rights reserved.
//

import Foundation

class Notifications {
    static let shared = Notifications()
    private init() {  }
    
    func setUpNotifications() {
        NotificationCenter.default.addObserver(self, selector: #selector(didReceiveNotification(notif:)), name: NSNotification.Name.init(rawValue: "notification"), object: nil)
    }
    
    @objc func didReceiveNotification(notif: Notification) {
        guard let userInfo = notif.userInfo,
            let _ = notif.object as? String,
            let _  = userInfo["message"] as? String,
            let _     = userInfo["date"]    as? Date else {
                print("No userInfo found in notification")
                return
        }
    }
    
    func sendNotification() {
        let object = "TOm"
        let userInfo = ["message":"Hello there!", "date":Date()] as [String : Any]
        NotificationCenter.default.post(name: NSNotification.Name.init(rawValue: "notification"), object: object, userInfo: userInfo)
    }
    
    deinit {
        NotificationCenter.default.removeObserver(self)
    }
}
