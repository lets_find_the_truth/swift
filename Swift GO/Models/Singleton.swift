//
//  Singelton.swift
//  Swift GO
//
//  Created by Kyryl Nevedrov on 21/03/17.
//  Copyright © 2017 Kyryl Nevedrov. All rights reserved.
//

import Foundation

class Singleton {
    static let shared = Singleton()
    private init() {}
}
