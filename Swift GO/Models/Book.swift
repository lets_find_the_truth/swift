//
//  Reader.swift
//  Swift GO
//
//  Created by Kyryl Nevedrov on 22/03/17.
//  Copyright © 2017 Kyryl Nevedrov. All rights reserved.
//

import Foundation

protocol PageDelegate {
    func countOfPage() -> Int
}

class Book {
    var delegate: PageDelegate?
    
    func printPageNumber() {
        if delegate != nil {
            print(delegate!.countOfPage())
        }
    }
}

class Page: PageDelegate {
    let pageNumber = 123
    
    func countOfPage() -> Int {
        return 343
    }
}

//USAGE
//let book = Book()
//let page = Page()
//book.delegate = page
//book.printPageNumber()

