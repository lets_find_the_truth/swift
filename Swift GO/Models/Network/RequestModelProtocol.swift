//
//  RequestModelProtocol.swift
//  network
//
//  Created by Kyryl Nevedrov on 26/07/17.
//  Copyright © 2017 Kyryl Nevedrov. All rights reserved.
//

import Foundation
import Alamofire

protocol RequestModelProtocol {
    var URL: String { get  set }
    var method: HTTPMethod { get set }
    var parameters: [String: Any]? { get set }
    var headers: HTTPHeaders? { get set }
    var successCodes: [Int] { get set }
}
