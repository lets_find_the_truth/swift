//
//  RequestHandlerProtocol.swift
//  mvp-template
//
//  Created by Nevedrov Kyryl on 26/07/17.
//  Copyright © 2017 Vedidev. All rights reserved.
//

import Foundation

protocol RequestHandlerProtocol {
    func parseJSON(JSON: Any) throws
}
