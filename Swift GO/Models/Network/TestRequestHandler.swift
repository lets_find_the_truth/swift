//
//  TestRequestHandler.swift
//  mvp-template
//
//  Created by Nevedrov Kyryl on 26/07/17.
//  Copyright © 2017 Vedidev. All rights reserved.
//

import Foundation

class TestRequestHandler: RequestHandlerProtocol {
    var object: TestClassForHandler?
    
    func parseJSON(JSON: Any) throws {
        guard let dictionary = JSON as? NSDictionary else {
            throw NetworkErrorType.JSONParseError
        }
        
        self.object = try TestClassForHandler(dictionary)
    }
}
