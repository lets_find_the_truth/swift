//
//  TestRequestHandler.swift
//  network
//
//  Created by Kyryl Nevedrov on 26/07/17.
//  Copyright © 2017 Kyryl Nevedrov. All rights reserved.
//

import Foundation

class TestClassForHandler {
    var url: String
    
    init(_ dictionary: NSDictionary) throws {
        guard let url = dictionary.object(forKey: "url") as? String else {
            throw NetworkErrorType.JSONParseError
        }
        
        self.url = url
    }
}
