//
//  NetworkManager.swift
//  network
//
//  Created by Kyryl Nevedrov on 26/07/17.
//  Copyright © 2017 Kyryl Nevedrov. All rights reserved.
//

import Foundation
import Alamofire

protocol CustomErrorProtocol: Error {
    var localizedDescription: String { get }
    var localizedTitle: String { get }
    var code: Int { get }
}

class NetworkError: CustomErrorProtocol {
    var localizedDescription: String
    var localizedTitle: String
    var code: Int
    
    init(localizedDescription: String, localizedTitle: String, code: Int) {
        self.localizedDescription = localizedDescription
        self.localizedTitle = localizedTitle
        self.code = code
    }
}

enum NetworkErrorType: Error {
    case JSONParseError
}

class NetworkManager {
    static let shared = NetworkManager()
    
    private var manager: Alamofire.SessionManager!
    
    private init() {
        let configuration = URLSessionConfiguration.default
        configuration.timeoutIntervalForRequest = 5
        self.manager = Alamofire.SessionManager(configuration: configuration)
    }
    
    
    func request<T: RequestHandlerProtocol>(model: RequestModelProtocol, handler: T ,completion: @escaping (T, NetworkError?) -> Void) {
        
        // JSON parsing function
        func parseJSON(_ response: DataResponse<Any>) {
            switch response.result {
            case .success:
                do {
                    try handler.parseJSON(JSON: response.result.value!)
                } catch {
                    completion(handler, NetworkError(localizedDescription: "Parse error", localizedTitle: "", code: 0))
                }
                
                completion(handler, nil)
            case .failure:
                completion(handler, NetworkError(localizedDescription: "Status code not \(model.successCodes)", localizedTitle: "", code: response.response!.statusCode))
            }
        }
        
        switch model.method {
        case .get:
            let url = URL(string: model.URL)
            var urlRequest = URLRequest(url: url!)
            urlRequest.httpMethod = model.method.rawValue
            for (key, value) in model.headers ?? [:] {
                urlRequest.addValue(value, forHTTPHeaderField: key)
            }
            
            self.manager.request(urlRequest).validate(statusCode: model.successCodes).responseJSON(completionHandler: { (response) in
                parseJSON(response)
            })
        case .post, .put, .delete:
            self.manager.request(model.URL, method: model.method, parameters: model.parameters, encoding: JSONEncoding.default, headers: model.headers).validate(statusCode: model.successCodes).responseJSON { (response) in
                parseJSON(response)
            }
        default:
            break
        }
    }
}
