//
//  TestModel.swift
//  network
//
//  Created by Kyryl Nevedrov on 26/07/17.
//  Copyright © 2017 Kyryl Nevedrov. All rights reserved.
//

import Foundation
import Alamofire

class TestModel: RequestModelProtocol {
    var URL: String = ""
    var method: HTTPMethod
    var parameters: [String : Any]?
    var headers: HTTPHeaders?
    var successCodes: [Int]
 
    var parameter: String
    
    init(parameter: String) {
        self.parameter = parameter
        
        // set method and success code range
        self.method = .get
        self.successCodes = [200]
        completeParameters()
    }
    
    private func completeParameters() {
        self.URL = "https://httpbin.org/get"
        
//        self.parameters = [
//           // "email": parameter
//        ]
        
//        self.headers = [
//           // "Authorization": "Bearer \(parameter)"
//        ]
    }
}
