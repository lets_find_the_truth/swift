//
//  Extensions.swift
//  Swift GO
//
//  Created by Kyryl Nevedrov on 19/09/17.
//  Copyright © 2017 Kyryl Nevedrov. All rights reserved.
//

import Foundation

extension String {
    func isValidEmail() -> Bool {
        let emailRegEx = "[A-Z0-9a-z._%+-]+@[A-Za-z0-9.-]+\\.[A-Za-z]{2,4}"
        let emailTest = NSPredicate(format:"SELF MATCHES %@", emailRegEx)
        let result = emailTest.evaluate(with: self)
        return result
    }
    
    func isValidPassword() -> Bool {
        let passRegEx = "^.{6,}$"
        let passTest = NSPredicate(format:"SELF MATCHES %@", passRegEx)
        let result = passTest.evaluate(with: self)
        return result
    }
}
