//
//  ReadWriteFile.swift
//  Swift GO
//
//  Created by Kyryl Nevedrov on 21/03/17.
//  Copyright © 2017 Kyryl Nevedrov. All rights reserved.
//

import Foundation

class ReadWriteFile {
    private let file = "file.log"
    
    func writeStringToFile(text: String) {
        if let dir = FileManager.default.urls(for: .documentDirectory, in: .userDomainMask).first {
            let path = dir.appendingPathComponent(file)
            
            do {
                try text.write(to: path, atomically: false, encoding: String.Encoding.utf8)
            } catch {  }
        }
    }
    
    func readStringFromFile() -> String {
        if let dir = FileManager.default.urls(for: .documentDirectory, in: .userDomainMask).first {
            let path = dir.appendingPathComponent(file)
            
            do {
                let text = try String(contentsOf: path, encoding: String.Encoding.utf8)
                return text
            } catch {}
        }
        return ""
    }
}
