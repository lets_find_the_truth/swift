//
//  LocalNotification.swift
//  Swift GO
//
//  Created by Nevedrov Kyryl on 24/05/17.
//  Copyright © 2017 Kyryl Nevedrov. All rights reserved.
//

import Foundation
import UserNotifications

class LocalNotification {
    func showNotification() {
        let center = UNUserNotificationCenter.current()
        
        let content = UNMutableNotificationContent()
        content.title = "title"
        content.body = "this is me"
        content.sound = UNNotificationSound.default()
        
        let action = UNNotificationAction(identifier: "identifier action", title: "OK", options: [])
        let category = UNNotificationCategory(identifier: "category", actions: [action], intentIdentifiers: [], options: [])
        content.categoryIdentifier = "category"
        
        center.setNotificationCategories([category])
        
        let trigger = UNTimeIntervalNotificationTrigger(timeInterval: 0.1, repeats: false)
        
        let request = UNNotificationRequest(identifier: "request", content: content, trigger: trigger)
        
        center.add(request) { (error) in
            if error != nil {
                print("notification error")
            }
        }
    }
}
