//
//  MailViewController.swift
//  Swift GO
//
//  Created by Kyryl Nevedrov on 21/03/17.
//  Copyright © 2017 Kyryl Nevedrov. All rights reserved.
//

//added MessagesUI kit to frameworks
import UIKit
import MessageUI

class MailViewController: UIViewController, MFMailComposeViewControllerDelegate {

    override func viewDidLoad() {
        super.viewDidLoad()

        // Do any additional setup after loading the view.
    }
    
    func sendMail() {
        if MFMailComposeViewController.canSendMail() {
            let mailController = MFMailComposeViewController()
            mailController.mailComposeDelegate = self
            mailController.setSubject("Subject")
            mailController.setMessageBody("", isHTML: true)
            let dataToSend = Data()
            mailController.addAttachmentData(dataToSend, mimeType:"text/plain", fileName:"ble.log")
            
            present(mailController, animated: true, completion: nil)
            
        } else {
            Utilities.showAlertController(title: "", message: "You can't send mail", completion: nil)
        }
    }

    func mailComposeController(_ controller: MFMailComposeViewController, didFinishWith result: MFMailComposeResult, error: Error?) {
        controller.dismiss(animated: true, completion: nil)
    }

}
