//
//  AllUIElementsViewController.swift
//  Swift GO
//
//  Created by Kyryl Nevedrov on 28/03/17.
//  Copyright © 2017 Kyryl Nevedrov. All rights reserved.
//

import UIKit

class AllUIElementsViewController: UIViewController {
    @IBOutlet weak var buttonLabel: UILabel!
    @IBOutlet weak var segmentLabel: UILabel!
    @IBOutlet weak var textFieldLabel: UILabel!
    @IBOutlet weak var switchLabel: UILabel!
    @IBOutlet weak var sliderLabel: UILabel!
    @IBOutlet weak var activityIndicatorLabel: UILabel!
    @IBOutlet weak var progressViewLabel: UILabel!
    @IBOutlet weak var stepperLabel: UILabel!
    @IBOutlet weak var datePickerLabel: UILabel!
    @IBOutlet weak var pickerLabel: UILabel!
    @IBOutlet weak var segmentControl: UISegmentedControl!
    @IBOutlet weak var activityIndicator: UIActivityIndicatorView!
    
    @IBOutlet weak var switchGO: UISwitch!
    @IBOutlet weak var stepper: UIStepper!
    @IBOutlet weak var progressView: UIProgressView!
    @IBOutlet weak var slider: UISlider!
    @IBOutlet weak var textField: UITextField!
    @IBOutlet weak var datePicker: UIDatePicker!
    @IBOutlet weak var picker: UIPickerView!
    
    var activityIsRunning: Bool = false
    
    var pickerArray:[String] = ["My", "Love", "is"]
    
    @IBAction func didChangeValueStepper(_ sender: Any) {
         self.stepperLabel.text = "\(stepper.value)"
    }
    @IBAction func changeDate(_ sender: Any) {
        datePickerLabel.text = "\(datePicker.date)"
    }
    
    @IBAction func didTapButton(_ sender: Any) {
        activityIsRunning ? self.activityIndicator.stopAnimating() : self.activityIndicator.startAnimating()
        activityIsRunning = !activityIsRunning
    }
    
    @IBAction func didChangeSegmentedControlState(_ sender: Any) {
               self.segmentLabel.text = "\(self.segmentControl.selectedSegmentIndex)"
    }
    @IBAction func didTapSwitch(_ sender: Any) {
        self.switchLabel.text = "\(self.switchGO.isOn)"
    }

    @IBAction func didSlideSlider(_ sender: Any) {
        self.sliderLabel.text = "\(self.slider.value)"
        self.progressView.progress = self.slider.value
    }
    override func viewDidLoad() {
        super.viewDidLoad()
        self.stepperLabel.text = "\(stepper.value)"
        self.segmentLabel.text = "\(self.segmentControl.selectedSegmentIndex)"
        
        // Do any additional setup after loading the view.
    }

    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }
    

    /*
    // MARK: - Navigation

    // In a storyboard-based application, you will often want to do a little preparation before navigation
    override func prepare(for segue: UIStoryboardSegue, sender: Any?) {
        // Get the new view controller using segue.destinationViewController.
        // Pass the selected object to the new view controller.
    }
    */

}
extension AllUIElementsViewController: UIPickerViewDataSource, UIPickerViewDelegate {
    func numberOfComponents(in pickerView: UIPickerView) -> Int {
        return 1
    }
    
    func pickerView(_ pickerView: UIPickerView, numberOfRowsInComponent component: Int) -> Int {
        return pickerArray.count
    }
    
    func pickerView(_ pickerView: UIPickerView, titleForRow row: Int, forComponent component: Int) -> String? {
        return pickerArray[row]
    }
    
    func pickerView(_ pickerView: UIPickerView, didSelectRow row: Int, inComponent component: Int) {
        self.pickerLabel.text = "\(self.pickerArray[row])"
    }
}
