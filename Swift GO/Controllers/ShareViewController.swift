//
//  ShareViewController.swift
//  Swift GO
//
//  Created by Kyryl Nevedrov on 29/03/17.
//  Copyright © 2017 Kyryl Nevedrov. All rights reserved.
//

import UIKit

class ShareViewController: UIViewController {

    override func viewDidLoad() {
        super.viewDidLoad()
    }
    
    override func viewDidAppear(_ animated: Bool) {
        let textToShare = "Share it"
        
        if let imageToShare = UIImage(named: "imageGO") {
            shareTextAndImage(text: textToShare, image: imageToShare)
        }
    }
   
    func shareTextAndImage(text: String, image: UIImage) {
        let activityController = UIActivityViewController(activityItems: [text, image], applicationActivities: nil)
        activityController.popoverPresentationController?.sourceView = self.view //so that’s ipad won’t crash
        present(activityController, animated: true, completion: nil)
    }
}
