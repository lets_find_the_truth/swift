//
//  userDefaults.swift
//  Swift GO
//
//  Created by Kyryl Nevedrov on 23/03/17.
//  Copyright © 2017 Kyryl Nevedrov. All rights reserved.
//

import Foundation

class UsingUserDefaults {
    static let shared = UsingUserDefaults()
    
    private init() {}
    
    func saveValue(value: Any, forKey: String) {
        UserDefaults.standard.set(value, forKey: forKey)
    }
    
    func retrieveValue(forKey: String) -> Any? {
        return UserDefaults.standard.value(forKey: forKey)
    }
    
    
}
