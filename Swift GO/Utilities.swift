//
//  Utilities.swift
//  Swift GO
//
//  Created by Kyryl Nevedrov on 21/03/17.
//  Copyright © 2017 Kyryl Nevedrov. All rights reserved.
//

import UIKit

class Utilities {
    static func showAlertController(title: String, message: String, completion: ((UIAlertAction) -> Void)?) {
        let rootViewController = UIApplication.shared.keyWindow!.rootViewController
        
        let alertController = UIAlertController(title: title, message: message, preferredStyle: .alert)
        let okAction = UIAlertAction(title: "OK", style: .cancel, handler: completion)
        alertController.addAction(okAction)
        
        rootViewController?.present(alertController, animated: true, completion: nil)
    }
}
